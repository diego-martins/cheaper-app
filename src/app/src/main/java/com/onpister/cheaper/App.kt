package com.onpister.cheaper

import android.app.Application
import com.google.android.gms.ads.MobileAds
import com.crashlytics.android.Crashlytics
import com.google.firebase.FirebaseApp
import com.google.firebase.database.FirebaseDatabase
import io.fabric.sdk.android.Fabric

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        if(!BuildConfig.DEBUG) {
            Fabric.with(this, Crashlytics())
        }
        MobileAds.initialize(this);

        if (!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }
    }
}