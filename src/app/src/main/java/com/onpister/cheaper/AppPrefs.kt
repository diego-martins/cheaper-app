package com.onpister.cheaper

import android.content.Context
import android.content.SharedPreferences

class AppPrefs(context: Context){
    val PREFS_FILENAME = "com.onpister.cheaper.app_prefs"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)

    var showedVoiceTip: Boolean
        get() = prefs.getBoolean("showed_voice_tip", false)
        set(value) = prefs.edit().putBoolean("showed_voice_tip", value).apply()

    var showedVoiceScreen: Boolean
        get() = prefs.getBoolean("showed_voice_screen", false)
        set(value) = prefs.edit().putBoolean("showed_voice_screen", value).apply()

}