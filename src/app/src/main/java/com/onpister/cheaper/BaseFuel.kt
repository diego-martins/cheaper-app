package com.onpister.cheaper

open class BaseFuel(
        var fuelType: FuelType = FuelType.NONE,
        var price: Double = 0.0,
        var percentCheaper: Double = 0.0,
        var showType: ShowType = ShowType.NONE,
        var betterThen: ArrayList<Pair<FuelType, Double>> = ArrayList<Pair<FuelType, Double>>()
)