package com.onpister.cheaper

enum class FuelType {
    NONE,
    GASOLINE,
    ETHANOL,
    DIESEL,
    GNV,
    ELETRIC
}