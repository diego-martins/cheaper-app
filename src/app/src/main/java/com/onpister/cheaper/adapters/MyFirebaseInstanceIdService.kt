package com.onpister.cheaper.adapters

import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.messaging.FirebaseMessaging
import com.onpister.cheaper.BuildConfig
import com.onpister.cheaper.UserPrefs
import java.util.*

class MyFirebaseInstanceIdService : FirebaseInstanceIdService() {
    private val TAG = MyFirebaseInstanceIdService::class.java!!.getSimpleName()
    override fun onTokenRefresh() {
        try {
            val firebaseMessaging = FirebaseMessaging.getInstance()
            if (firebaseMessaging != null) {
                if(BuildConfig.DEBUG) {
                    firebaseMessaging.subscribeToTopic("dev")
                }
                var userPrefs = UserPrefs(this)
                if(userPrefs.notification) {
                    firebaseMessaging.subscribeToTopic(Locale.getDefault().country.toLowerCase())
                }else{
                    firebaseMessaging.unsubscribeFromTopic(Locale.getDefault().country.toLowerCase())
                }
                firebaseMessaging.subscribeToTopic("everyone")

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onTokenRefresh()
    }

}