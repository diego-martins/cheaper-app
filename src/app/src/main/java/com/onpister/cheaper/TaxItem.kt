package com.onpister.cheaper

open class TaxItem(
        var name: String = "",
        var description: String = "",
        var percent: Double = 0.0,
        var value: Double = 0.0
)