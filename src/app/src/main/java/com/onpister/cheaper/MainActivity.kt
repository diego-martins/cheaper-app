package com.onpister.cheaper

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.NativeExpressAdView
import com.onpister.cheaper.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v7.content.res.AppCompatResources
import android.view.*
import android.app.Activity
import android.graphics.Bitmap
import android.support.v4.content.FileProvider
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetView
import com.google.android.gms.ads.AdView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import kotlinx.android.synthetic.main.action_bar.*
import java.io.File
import java.io.FileOutputStream
import java.util.*

class MainActivity : AppCompatActivity() {

    val PARAM_START_SPEECH = "START_SPEAK"
    val REQUEST_SETTINGS = 1
    var appPrefs: AppPrefs? = null
    var database = FirebaseDatabase.getInstance()
    var tax = Tax()
    var autonomy = Autonomy()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        appPrefs = AppPrefs(this)

        init()
        loadAutonomy()
        loadTax()
        /*
        if(!prefs!!.showedVoiceTip) {
            toolbar.postDelayed({
                showVoiceCommand()
            }, 500)
        }
        */
    }

    fun loadAutonomy(){
        var userPrefs = UserPrefs(this)
        autonomy.ethToGas = (userPrefs.autonomyKmEthanol/userPrefs.autonomyKmGasoline)
        autonomy.gnvToEth = (userPrefs.autonomyKmGNV/userPrefs.autonomyKmEthanol)
        autonomy.gnvToGas = (userPrefs.autonomyKmGNV/userPrefs.autonomyKmGasoline)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        if(BuildConfig.DEBUG){
            menu!!.findItem(R.id.action_speech).setVisible(true)
            menu!!.findItem(R.id.action_settings).setVisible(true)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_speech -> {
                var intent = Intent(this, SpeakActivity::class.java)
                if(!BuildConfig.DEBUG) {
                    if(appPrefs!!.showedVoiceScreen) {
                        intent.putExtra(PARAM_START_SPEECH, true)
                    }else{
                        appPrefs!!.showedVoiceScreen = true
                    }
                }
                startActivity(intent)

                return true
            }
            R.id.action_share -> {
                var shareIntent = Utils.Media.getShareIntent(this, getString(R.string.google_play_url))
                startActivity(shareIntent)
                return true
            }
            R.id.action_rate -> {
                Utils.Media.openWeb(this, "market://details?id=" + getPackageName())
                return true
            }
            R.id.action_settings -> {
                var intent = Intent(this, SettingsActivity::class.java)
                startActivityForResult(intent, REQUEST_SETTINGS)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    fun loadTax(){
        var ref = database.getReference("tax").child(Locale.getDefault().country)
        ref.keepSynced(true)
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                println(p0!!.message)
            }

            override fun onDataChange(snapshot: DataSnapshot?) {
                var res = snapshot!!.getValue<Tax>(Tax::class.java)
                if(res != null){
                    tax = res
                }
            }
        })
    }

    fun showVoiceCommand(){
        TapTargetView.showFor(
            this,
            TapTarget.forToolbarMenuItem(toolbar, R.id.action_speech, getString(R.string.tip_voice_title), getString(R.string.tip_voice_text)),
            object: TapTargetView.Listener() {
                override fun onTargetDismissed(view: TapTargetView?, userInitiated: Boolean) {
                    appPrefs!!.showedVoiceTip = true
                    super.onTargetDismissed(view, userInitiated)
                }
            });
    }

    fun init(){

        val leftGasDrawable = AppCompatResources.getDrawable(this, R.drawable.ic_gas_station)
        inputGasoline.setCompoundDrawablesWithIntrinsicBounds(leftGasDrawable, null, null, null)
        inputGasoline.setText(Utils.Text.formattString("0"))
        inputGasoline.setOnFocusChangeListener { view, b ->
            inputGasoline.setSelection(inputGasoline.text.length)
            if(inputGasolineLayout.tag != null)
                inputGasolineLayout.error = inputGasolineLayout.tag as String
            inputGasolineLayout.isErrorEnabled = b && !inputGasolineLayout.error.isNullOrEmpty()
        }
        inputGasoline.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputGasoline.removeTextChangedListener(this)
                inputGasoline.setText(Utils.Text.formattString(s.toString()))
                inputGasoline.setSelection(inputGasoline.text.length)
                inputGasoline.addTextChangedListener(this)
            }
            override fun afterTextChanged(s: Editable?) {
                autoCalc()
            }
        })

        inputEthanol.setCompoundDrawablesWithIntrinsicBounds(leftGasDrawable, null, null, null)
        inputEthanol.setText(Utils.Text.formattString("0"))
        inputEthanol.setOnFocusChangeListener { view, b ->
            inputEthanol.setSelection(inputEthanol.text.length)
            if(inputEthanolLayout.tag != null)
                inputEthanolLayout.error = inputEthanolLayout.tag as String
            inputEthanolLayout.isErrorEnabled = b && !inputEthanolLayout.error.isNullOrEmpty()
        }
        inputEthanol.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputEthanol.removeTextChangedListener(this)
                inputEthanol.setText(Utils.Text.formattString(s.toString()))
                inputEthanol.setSelection(inputEthanol.text.length)
                inputEthanol.addTextChangedListener(this)
            }
            override fun afterTextChanged(s: Editable?) {
                autoCalc()
            }
        })

        val leftGasCylinderDrawable = AppCompatResources.getDrawable(this, R.drawable.ic_gas_cylinder)
        inputGNV.setCompoundDrawablesWithIntrinsicBounds(leftGasCylinderDrawable, null, null, null)
        inputGNV.setText(Utils.Text.formattString("0"))
        inputGNV.setOnFocusChangeListener { view, b ->
            inputGNV.setSelection(inputGNV.text.length)
            if(inputGNVLayout.tag != null)
                inputGNVLayout.error = inputGNVLayout.tag as String
            inputGNVLayout.isErrorEnabled = b && !inputGNVLayout.error.isNullOrEmpty()
        }
        inputGNV.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputGNV.removeTextChangedListener(this)
                inputGNV.setText(Utils.Text.formattString(s.toString()))
                inputGNV.setSelection(inputGNV.text.length)
                inputGNV.addTextChangedListener(this)
            }
            override fun afterTextChanged(s: Editable?) {
                autoCalc()
            }
        })
        inputGNV.setOnEditorActionListener { textView, i, keyEvent ->
            if(i == EditorInfo.IME_ACTION_DONE){
                inputGNV.clearFocus()
                val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
                adViewContainer.visibility = View.VISIBLE
            }
            true
        }

        loadAd()
    }

    fun loadAd(){

        var remoteConfig = FirebaseRemoteConfig.getInstance()
        var adEnabled = remoteConfig.getBoolean("ad_enabled")
        var adType = remoteConfig.getString("ad_type")
        var adProvider = remoteConfig.getString("ad_provider")

        if(!adEnabled) return

        if(adProvider.equals("admob")) {
            if(adType.equals("native")){
                var adView = NativeExpressAdView(this)
                adView.adUnitId = getString(R.string.ad_admob_native)
                adView.adSize = AdSize(AdSize.FULL_WIDTH, 80)
                adView.setLayoutParams(ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
                adView.adListener = object : AdListener() {
                    override fun onAdLoaded() {
                        adViewContainer.removeAllViews()
                        adViewContainer.addView(adView)
                        super.onAdLoaded()
                    }
                }
                adView.loadAd(Utils.Ad.adRequest())
            }
            else if(adType.equals("banner")){
                var adView = AdView(this)
                adView.adUnitId = getString(R.string.ad_admob_banner)
                adView.adSize = AdSize.SMART_BANNER
                adView.setLayoutParams(ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
                adView.adListener = object : AdListener() {
                    override fun onAdLoaded() {
                        adViewContainer.removeAllViews()
                        adViewContainer.addView(adView)
                        super.onAdLoaded()
                    }
                }
                adView.loadAd(Utils.Ad.adRequest())
            }
        }
    }

    fun autoCalc(){

        listResult.removeAllViews()

        var gas: Double = Utils.Text.formattDecimal(inputGasoline.text.toString())
        var ethanol: Double = Utils.Text.formattDecimal(inputEthanol.text.toString())
        var gnv: Double = Utils.Text.formattDecimal(inputGNV.text.toString())

        if (gas > 0.009) {
            var totalTax = getTax(gas, FuelType.GASOLINE)
            if(totalTax > 0.009) {
                inputGasolineLayout.error = getString(R.string.label_total_tax) + " ~" + Utils.Text.formattString(totalTax)
                inputGasolineLayout.tag = inputGasolineLayout.error
            }else{
                inputGasolineLayout.error = ""
                inputGasolineLayout.tag = inputGasolineLayout.error
            }
        } else {
            inputGasolineLayout.error = ""
            inputGasolineLayout.tag = inputGasolineLayout.error
        }

        if (ethanol > 0.009) {
            var totalTax = getTax(ethanol, FuelType.ETHANOL)
            if(totalTax > 0.009) {
                inputEthanolLayout.error = getString(R.string.label_total_tax) + " ~" + Utils.Text.formattString(totalTax)
                inputEthanolLayout.tag = inputEthanolLayout.error
            }else{
                inputEthanolLayout.error = ""
                inputEthanolLayout.tag = inputEthanolLayout.error
            }
        } else {
            inputEthanolLayout.error = ""
            inputEthanolLayout.tag = inputEthanolLayout.error
        }

        if (gnv > 0.009) {
            var totalTax = getTax(gnv, FuelType.GNV)
            if(totalTax > 0.009) {
                inputGNVLayout.error = getString(R.string.label_total_tax) + " ~" + Utils.Text.formattString(totalTax)
                inputGNVLayout.tag = inputGNVLayout.error
            }else{
                inputGNVLayout.error = ""
                inputGNVLayout.tag = inputGNVLayout.error
            }
        } else {
            inputGNVLayout.error = ""
            inputGNVLayout.tag = inputGNVLayout.error
        }

        if(inputGasoline.isFocused && !inputGasolineLayout.error.isNullOrEmpty()){
            inputEthanolLayout.isErrorEnabled = false
            inputGNVLayout.isErrorEnabled = false
            inputGasolineLayout.isErrorEnabled = true
        }
        else if(inputEthanol.isFocused && !inputEthanolLayout.error.isNullOrEmpty()){
            inputGasolineLayout.isErrorEnabled = false
            inputGNVLayout.isErrorEnabled = false
            inputEthanolLayout.isErrorEnabled = true
        }
        else if(inputGNV.isFocused && !inputGNVLayout.error.isNullOrEmpty()){
            inputGasolineLayout.isErrorEnabled = false
            inputEthanolLayout.isErrorEnabled = false
            inputGNVLayout.isErrorEnabled = true
        }else{
            inputGasolineLayout.isErrorEnabled = false
            inputEthanolLayout.isErrorEnabled = false
            inputGNVLayout.isErrorEnabled = false
        }

        if(gas.compareTo(0) != 0 || ethanol.compareTo(0) != 0 || gnv.compareTo(0) != 0){

            var res = Utils.Calc.result(
                    BaseFuel(FuelType.GASOLINE, gas),
                    BaseFuel(FuelType.ETHANOL, ethanol),
                    BaseFuel(FuelType.GNV, gnv),
                    autonomy)

            showResults(res)

        }
    }

    fun showTax(view: View, price: Double, fuelType: FuelType){

        var viewTax = view.findViewById<View>(R.id.viewTax)
        viewTax.visibility = View.GONE

        if(tax != null){
            var taxList = ArrayList<TaxItem>()

            if(fuelType == FuelType.GASOLINE){
                taxList = tax.gasoline
            }
            else if(fuelType == FuelType.ETHANOL){
                taxList = tax.ethanol
            }
            else if(fuelType == FuelType.GNV){
                taxList = tax.gnv
            }

            if(taxList.isNotEmpty()) {
                var sb = StringBuilder()
                sb.append("~" + Locale.getDefault().country + " -");
                var percent: Double = 0.0
                var value: Double = 0.0
                taxList.forEach(fun (taxItem: TaxItem){
                    if(taxItem != null && !taxItem.name.isNullOrEmpty() && (taxItem.percent > 0 || taxItem.value > 0)) {
                        sb.append(" " + taxItem.name + ": ")
                        if (taxItem.percent > 0) {
                            sb.append(String.format("%.1f%%", taxItem.percent))
                            percent += taxItem.percent
                        } else if (taxItem.value > 0) {
                            sb.append(Utils.Text.formattString(taxItem.value))
                            value += taxItem.value
                        }
                    }
                })

                if(percent > 0 || value > 0){
                    var priceWithoutTax = price
                    if(percent > 0){
                        priceWithoutTax = priceWithoutTax - (priceWithoutTax*percent)/100
                    }

                    if(value > 0 && value <= priceWithoutTax){
                        priceWithoutTax -= value
                    }
                    if(priceWithoutTax != price){
                        view.findViewById<TextView>(R.id.labelTaxResult).text = Utils.Text.formattString(price - priceWithoutTax)
                        view.findViewById<TextView>(R.id.labelWithoutTaxResult).text = Utils.Text.formattString(priceWithoutTax)
                        view.findViewById<TextView>(R.id.labelTaxDetails).text = sb.toString()
                        viewTax.visibility = View.VISIBLE
                    }
                }

            }

        }

    }

    fun getTax(price: Double, fuelType: FuelType): Double{

        if(tax != null){
            var taxList = ArrayList<TaxItem>()

            if(fuelType == FuelType.GASOLINE){
                taxList = tax.gasoline
            }
            else if(fuelType == FuelType.ETHANOL){
                taxList = tax.ethanol
            }
            else if(fuelType == FuelType.GNV){
                taxList = tax.gnv
            }

            if(taxList.isNotEmpty()) {
                var percent: Double = 0.0
                var value: Double = 0.0
                taxList.forEach(fun (taxItem: TaxItem){
                    if(taxItem != null && !taxItem.name.isNullOrEmpty() && (taxItem.percent > 0 || taxItem.value > 0)) {
                        if (taxItem.percent > 0) {
                            percent += taxItem.percent
                        } else if (taxItem.value > 0) {
                            value += taxItem.value
                        }
                    }
                })

                if(percent > 0 || value > 0){
                    var priceWithoutTax = price
                    if(percent > 0){
                        priceWithoutTax = priceWithoutTax - (priceWithoutTax*percent)/100
                    }

                    if(value > 0 && value <= priceWithoutTax){
                        priceWithoutTax -= value
                    }

                    if(priceWithoutTax != price){
                        return price - priceWithoutTax
                    }
                }

            }

        }
        return 0.0
    }

    fun shareIt(view: View){
        view.findViewById<ImageButton>(R.id.btnShare).visibility = View.GONE
        var bitmap = Utils.Image.viewToImage(view.findViewById<View>(R.id.cardView))
        view.findViewById<ImageButton>(R.id.btnShare).visibility = View.VISIBLE
        if(bitmap != null){
            var file = File(cacheDir, "cheaper_result.png")
            try {
                var stream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                stream.close()

                var contentUri = FileProvider.getUriForFile(this,  BuildConfig.APPLICATION_ID, file)
                if(contentUri != null) {
                    startActivity(Utils.Media.getShareIntent(this, "", getString(R.string.google_play_url), contentUri))
                }

            }catch (e: Exception){
                e.printStackTrace()
                Toast.makeText(this, R.string.error_share, Toast.LENGTH_SHORT).show()
            }
        }else{
        }
    }

    fun showResult(res: BaseFuel){

        if(res.showType == ShowType.BEST) {

            if (res.fuelType == FuelType.GASOLINE) {

                var view: View = layoutInflater.inflate(R.layout.item_result_g, null)
                view.findViewById<TextView>(R.id.labelTitle).text = getString(R.string.label_max_price) + " " + getString(R.string.gasoline)
                view.findViewById<TextView>(R.id.labelResult).text = Utils.Text.formattString(res.price)
                if(res.betterThen[0].first == FuelType.ETHANOL) {
                    view.findViewById<TextView>(R.id.labelPercent).text =  String.format("%s/%s %.1f%%", getString(R.string.abb_ethanol), getString(R.string.abb_gasoline), res.betterThen[0].second*100)
                }else{
                    view.findViewById<TextView>(R.id.labelPercent).text =  String.format("%s/%s %.1f%%", getString(R.string.abb_gnv), getString(R.string.abb_gasoline), res.betterThen[0].second*100)
                }
                //labelPercent1.text = String.format("-%.1f%%", res.percentCheaper)
                view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
                showTax(view, res.price, res.fuelType)
                view.findViewById<ImageButton>(R.id.btnShare).setOnClickListener {
                    shareIt(view)
                }
                listResult.addView(view)

            }
            else if (res.fuelType == FuelType.ETHANOL) {

                var view: View = layoutInflater.inflate(R.layout.item_result_e, null)
                view.findViewById<TextView>(R.id.labelTitle).text = getString(R.string.label_max_price) + " " + getString(R.string.ethanol)
                view.findViewById<TextView>(R.id.labelResult).text = Utils.Text.formattString(res.price)
                if(res.betterThen[0].first == FuelType.GASOLINE) {
                    view.findViewById<TextView>(R.id.labelPercent).text =  String.format("%s/%s %.1f%%", getString(R.string.abb_ethanol), getString(R.string.abb_gasoline), res.betterThen[0].second*100)
                }else{
                    view.findViewById<TextView>(R.id.labelPercent).text =  String.format("%s/%s %.1f%%", getString(R.string.abb_gnv), getString(R.string.abb_ethanol), res.betterThen[0].second*100)
                }
                //labelPercent1.text = String.format("-%.1f%%", res.percentCheaper)
                view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
                showTax(view, res.price, res.fuelType)
                view.findViewById<ImageButton>(R.id.btnShare).setOnClickListener {
                    shareIt(view)
                }
                listResult.addView(view)
            }
            else if (res.fuelType == FuelType.GNV) {

                var view: View = layoutInflater.inflate(R.layout.item_result_gnv, null)
                view.findViewById<TextView>(R.id.labelTitle).text = getString(R.string.label_max_price) + " " + getString(R.string.gnv)
                view.findViewById<TextView>(R.id.labelResult).text = Utils.Text.formattString(res.price)
                if(res.betterThen[0].first == FuelType.GASOLINE) {
                    view.findViewById<TextView>(R.id.labelPercent).text =  String.format("%s/%s %.1f%%", getString(R.string.abb_gnv), getString(R.string.abb_gasoline), res.betterThen[0].second*100)
                }else{
                    view.findViewById<TextView>(R.id.labelPercent).text =  String.format("%s/%s %.1f%%", getString(R.string.abb_gnv), getString(R.string.abb_ethanol), res.betterThen[0].second*100)
                }
                //labelPercent1.text = String.format("-%.1f%%", res.percentCheaper)
                view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
                showTax(view, res.price, res.fuelType)
                view.findViewById<ImageButton>(R.id.btnShare).setOnClickListener {
                    shareIt(view)
                }
                listResult.addView(view)

            }

        }
        else if(res.showType == ShowType.COMPARE){

            if (res.fuelType == FuelType.GASOLINE) {

                var view: View = layoutInflater.inflate(R.layout.item_result_g, null)
                view.findViewById<TextView>(R.id.labelTitle).text = getString(R.string.label_best_choice)
                view.findViewById<TextView>(R.id.labelResult).text = getString(R.string.gasoline)
                if(res.betterThen[0].first == FuelType.ETHANOL) {
                    view.findViewById<TextView>(R.id.labelPercent).text =  String.format("%s/%s %.1f%%", getString(R.string.abb_ethanol), getString(R.string.abb_gasoline), res.betterThen[0].second*100)
                    if(res.betterThen[0].second <= autonomy.ethToGas) {
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_down), null, null, null)
                    }else{
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_up), null, null, null)
                    }
                }else{
                    view.findViewById<TextView>(R.id.labelPercent).text =  String.format("%s/%s %.1f%%", getString(R.string.abb_gnv), getString(R.string.abb_gasoline), res.betterThen[0].second*100)
                    if(res.betterThen[0].second <= autonomy.gnvToGas) {
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_down), null, null, null)
                    }else{
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_up), null, null, null)
                    }
                }
                showTax(view, res.price, res.fuelType)
                view.findViewById<ImageButton>(R.id.btnShare).setOnClickListener {
                    shareIt(view)
                }
                listResult.addView(view)

            }
            else if (res.fuelType == FuelType.ETHANOL) {

                var view: View = layoutInflater.inflate(R.layout.item_result_e, null)
                view.findViewById<TextView>(R.id.labelTitle).text = getString(R.string.label_best_choice)
                view.findViewById<TextView>(R.id.labelResult).text = getString(R.string.ethanol)
                if(res.betterThen[0].first == FuelType.GASOLINE) {
                    view.findViewById<TextView>(R.id.labelPercent).text = String.format("%s/%s %.1f%%", getString(R.string.abb_ethanol), getString(R.string.abb_gasoline), res.betterThen[0].second*100)
                    if(res.betterThen[0].second <= autonomy.ethToGas) {
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_down), null, null, null)
                    }else{
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_up), null, null, null)
                    }
                }else{
                    view.findViewById<TextView>(R.id.labelPercent).text = String.format("%s/%s %.1f%%", getString(R.string.abb_gnv), getString(R.string.abb_ethanol), res.betterThen[0].second*100)
                    if(res.betterThen[0].second <= autonomy.gnvToEth) {
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_down), null, null, null)
                    }else{
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_up), null, null, null)
                    }
                }
                showTax(view, res.price, res.fuelType)
                view.findViewById<ImageButton>(R.id.btnShare).setOnClickListener {
                    shareIt(view)
                }
                listResult.addView(view)

            }
            else if (res.fuelType == FuelType.GNV) {

                var view: View = layoutInflater.inflate(R.layout.item_result_gnv, null)
                view.findViewById<TextView>(R.id.labelTitle).text = getString(R.string.label_best_choice)
                view.findViewById<TextView>(R.id.labelResult).text = getString(R.string.gnv)
                if(res.betterThen[0].first == FuelType.GASOLINE) {
                    view.findViewById<TextView>(R.id.labelPercent).text = String.format("%s/%s %.1f%%", getString(R.string.abb_gnv), getString(R.string.abb_gasoline),res.betterThen[0].second*100)
                    if(res.betterThen[0].second <= autonomy.gnvToGas) {
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_down), null, null, null)
                    }else{
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_up), null, null, null)
                    }
                }else{
                    view.findViewById<TextView>(R.id.labelPercent).text = String.format("%s/%s %.1f%%", getString(R.string.abb_gnv), getString(R.string.abb_ethanol),res.betterThen[0].second*100)
                    if(res.betterThen[0].second <= autonomy.gnvToEth) {
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_down), null, null, null)
                    }else{
                        view.findViewById<TextView>(R.id.labelPercent).setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(this, R.drawable.ic_trending_up), null, null, null)
                    }
                }
                showTax(view, res.price, res.fuelType)
                view.findViewById<ImageButton>(R.id.btnShare).setOnClickListener {
                    shareIt(view)
                }
                listResult.addView(view)

            }

        }
    }

    fun showResults(res: ArrayList<BaseFuel>){

        if(res.size == 1){
            showResult(res[0])
        }
        else if(res.size == 2){
            showResult(res[0])
            showResult(res[1])
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_SETTINGS){
            loadAutonomy()
            autoCalc()
        }
    }
}
