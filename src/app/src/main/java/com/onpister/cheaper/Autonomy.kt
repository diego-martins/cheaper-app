package com.onpister.cheaper

class Autonomy(
        var ethToGas: Double = 0.7,
        var gnvToEth: Double = 0.4,
        var gnvToGas: Double = 0.3
)