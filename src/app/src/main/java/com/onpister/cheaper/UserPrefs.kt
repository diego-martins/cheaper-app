package com.onpister.cheaper

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class UserPrefs(context: Context) {

    val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    var notification: Boolean
        get() = prefs.getBoolean("notification", true)
        set(value) = prefs.edit().putBoolean("notification", value).apply()

    var autonomyKmGasoline: Double
        get() = prefs.getString("autonomyKmGasoline", "10").toDouble()
        set(value) = prefs.edit().putString("autonomyKmGasoline", value.toString()).apply()

    var autonomyKmEthanol: Double
        get() = prefs.getString("autonomyKmEthanol", "7").toDouble()
        set(value) = prefs.edit().putString("autonomyKmEthanol", value.toString()).apply()

    var autonomyKmGNV: Double
        get() = prefs.getString("autonomyKmGNV", "3").toDouble()
        set(value) = prefs.edit().putString("autonomyKmGNV", value.toString()).apply()

}