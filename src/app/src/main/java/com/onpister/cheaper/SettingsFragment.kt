package com.onpister.cheaper

import android.os.Bundle
import android.preference.EditTextPreference
import android.preference.Preference
import android.preference.PreferenceFragment
import android.preference.SwitchPreference
import android.widget.Toast
import com.google.firebase.messaging.FirebaseMessaging
import java.util.*

class SettingsFragment : PreferenceFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferences)

        var notification = preferenceScreen.findPreference("notification") as SwitchPreference
        notification.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { preference, any ->
            loadFCM(any as Boolean)
            true
        }

        var autonomyKmGasoline = preferenceScreen.findPreference("autonomyKmGasoline") as EditTextPreference
        autonomyKmGasoline.onPreferenceChangeListener = validation()

        var autonomyKmEthanol = preferenceScreen.findPreference("autonomyKmEthanol") as EditTextPreference
        autonomyKmEthanol.onPreferenceChangeListener = validation()

        var autonomyKmGNV = preferenceScreen.findPreference("autonomyKmGNV") as EditTextPreference
        autonomyKmGNV.onPreferenceChangeListener = validation()

        var version = preferenceScreen.findPreference("version") as Preference
        version.title = getString(R.string.label_version) + " " + BuildConfig.VERSION_NAME

    }

    fun validation(): Preference.OnPreferenceChangeListener {
        return Preference.OnPreferenceChangeListener { preference, any ->
            var valueText = any as String?
            if(valueText.isNullOrEmpty()){
                Toast.makeText(activity, R.string.error_field_empty, Toast.LENGTH_SHORT).show()
                false
            }
            else{
                var value = valueText!!.toDouble()
                if(value <= 0 || value >= 50){
                    Toast.makeText(activity, R.string.error_field_invalid, Toast.LENGTH_SHORT).show()
                    false
                }
                else {
                    true
                }
            }
        }
    }

    fun loadFCM(enabled: Boolean){
        val firebaseMessaging = FirebaseMessaging.getInstance()
        if (firebaseMessaging != null) {
            if(BuildConfig.DEBUG) {
                firebaseMessaging.subscribeToTopic("dev")
            }
            var userPrefs = UserPrefs(activity)
            if(enabled) {
                firebaseMessaging.subscribeToTopic(Locale.getDefault().country.toLowerCase())
            }else{
                firebaseMessaging.unsubscribeFromTopic(Locale.getDefault().country.toLowerCase())
            }
            firebaseMessaging.subscribeToTopic("everyone")
        }
    }
}
