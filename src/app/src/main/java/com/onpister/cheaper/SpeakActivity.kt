package com.onpister.cheaper

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import kotlinx.android.synthetic.main.activity_speak.*
import java.util.*
import android.widget.Toast
import android.content.ActivityNotFoundException
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.NativeExpressAdView
import com.onpister.cheaper.utils.Utils
import kotlinx.android.synthetic.main.item_speak.view.*

class SpeakActivity : AppCompatActivity() {

    val REQUEST_SPEECH_INPUT = 100
    val PARAM_START_SPEECH = "START_SPEAK"
    var mAutonomy = Autonomy()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_speak)

        supportActionBar!!.elevation = 0f
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        btnSpeak.setOnClickListener {
            promptSpeechInput()
        }

        labelInputSpeak.text = ""
        labelResult.text = ""

        val startSpeech = intent.getBooleanExtra(PARAM_START_SPEECH, false)
        if(startSpeech){
            promptSpeechInput()
        }

        carouselView.pageCount = 3
        carouselView.setViewListener{
            position ->
            var view = layoutInflater.inflate(R.layout.item_speak, null)
            if(position == 0) {
                view.labelTitle.text = getString(R.string.msg_speak_title_1)
                view.labelText.text = getString(R.string.msg_speak_text_1)
            }else if(position == 1){
                view.labelTitle.text = getString(R.string.msg_speak_title_2)
                view.labelText.text = getString(R.string.msg_speak_text_2)
            }else{
                view.labelTitle.text = getString(R.string.msg_speak_title_3)
                view.labelText.text = getString(R.string.msg_speak_text_3)
            }
            view
        }

        loadAd()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_speak, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.action_calculator -> {
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_share -> {
                var shareIntent = Utils.Media.getShareIntent(this, getString(R.string.google_play_url))
                startActivity(shareIntent)
                return true
            }
            R.id.action_rate -> {
                Utils.Media.openWeb(this, "market://details?id=" + getPackageName())
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    fun loadAd(){
        var adView = NativeExpressAdView(this)
        adView.adUnitId = getString(R.string.ad_admob_native)
        adView.adSize = AdSize(AdSize.FULL_WIDTH, 80)
        adView.setLayoutParams(ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
        adView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                adViewContainer.removeAllViews()
                adViewContainer.addView(adView)
                super.onAdLoaded()
            }
        }
        adView.loadAd(Utils.Ad.adRequest())
    }

    fun promptSpeechInput(){
        var intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 3000);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS, 3000);
        //intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));

        try {
            startActivityForResult(intent, REQUEST_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(applicationContext,
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show()
        }

    }

    fun afterSpeech(speechText: String){

        var text: String = speechText.toLowerCase()
        var gasIndex = text.indexOf(getString(R.string.gasoline), 0, true)
        var ethanolIndex = text.indexOf(getString(R.string.ethanol), 0, true)
        carouselView.visibility = View.GONE

        if(gasIndex >= 0 && ethanolIndex >= 0){
            var priceGas: String
            var priceEthanol: String
            if(gasIndex < ethanolIndex){
                priceGas = text.substring(gasIndex, ethanolIndex).replace(Regex("\\D+"), "")
                priceEthanol = text.substring(ethanolIndex).replace("\\D+", "")
            }else{
                priceEthanol = text.substring(ethanolIndex, gasIndex).replace("\\D+", "")
                priceGas = text.substring(gasIndex).replace(Regex("\\D+"), "")
            }
            priceGas = priceGas.padEnd(3, '0')
            priceEthanol = priceEthanol.padEnd(3, '0')
            labelInputSpeak.text = getString(R.string.gasoline) + " ${Utils.Text.formattString(priceGas)} \n X \n " + getString(R.string.ethanol) + " ${Utils.Text.formattString(priceEthanol)}"
            calc(priceGas, priceEthanol)
        }
        else if(gasIndex >= 0){
            var priceGas = text.substring(gasIndex).replace(Regex("\\D+"), "")
            priceGas = priceGas.padEnd(3, '0')
            priceGas = Utils.Text.formattString(priceGas)
            labelInputSpeak.text = getString(R.string.gasoline) + " $priceGas"
            calc(priceGas, "")
        }
        else if(ethanolIndex >= 0){
            var priceEthanol = text.substring(ethanolIndex).replace(Regex("\\D+"), "")
            priceEthanol = priceEthanol.padEnd(3, '0')
            priceEthanol = Utils.Text.formattString(priceEthanol)
            labelInputSpeak.text = getString(R.string.ethanol) + " $priceEthanol"
            calc("", priceEthanol)
        }
        else{
            labelInputSpeak.text = getString(R.string.error_invalid_command)
            labelResult.text = ""
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            REQUEST_SPEECH_INPUT -> {
                if(resultCode == Activity.RESULT_OK && data != null){
                    val result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    afterSpeech(result[0])
                }
            }
        }
    }

    fun calc(gasText: String, ethanolText: String){

        var gas: Double = Utils.Text.formattDecimal(gasText)
        var ethanol: Double = Utils.Text.formattDecimal(ethanolText)
        labelResult.text = ""
        if(gas.compareTo(0) == 0 && ethanol.compareTo(0) == 0){
            Toast.makeText(this, R.string.error_require_fields, Toast.LENGTH_SHORT).show()
            return
        }

        var res: String = Utils.Calc.result(this, gas, ethanol, mAutonomy.ethToGas)
        labelResult.text = res
    }

}
