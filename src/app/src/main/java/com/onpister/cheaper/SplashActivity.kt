package com.onpister.cheaper

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import java.util.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false)
        loadRemoteConfig()
    }

    fun loadRemoteConfig(){
        var remoteConfig = FirebaseRemoteConfig.getInstance()
        var settings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()
        remoteConfig.setConfigSettings(settings)
        remoteConfig.setDefaults(R.xml.remote_config_defaults)

        var cacheExpiration: Long = 3600 // 1 hour in seconds.
        if(remoteConfig.info.configSettings.isDeveloperModeEnabled){
            cacheExpiration = 0
        }
        remoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, OnCompleteListener {
                    task ->
                    if (task.isSuccessful) {
                        remoteConfig.activateFetched()
                        println("remoteConfig fetch success!")
                    } else {
                        println("remoteConfig fetch failed!")
                    }
                    target(false)
                })
                .addOnFailureListener(this, OnFailureListener {
                    exception ->
                    println(exception.localizedMessage)
                    target(false)
                })

    }

    fun loadFCM(){
        val firebaseMessaging = FirebaseMessaging.getInstance()
        if (firebaseMessaging != null) {
            if(BuildConfig.DEBUG) {
                firebaseMessaging.subscribeToTopic("dev")
            }
            var userPrefs = UserPrefs(this)
            if(userPrefs.notification) {
                firebaseMessaging.subscribeToTopic(Locale.getDefault().country.toLowerCase())
            }else{
                firebaseMessaging.unsubscribeFromTopic(Locale.getDefault().country.toLowerCase())
            }
            firebaseMessaging.subscribeToTopic("everyone")
        }
    }

    fun target(wait: Boolean){

        Handler().postDelayed({
            loadFCM()
        }, 500)

        if(wait){
            Handler().postDelayed({
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }, 300)
        }else{
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

}
