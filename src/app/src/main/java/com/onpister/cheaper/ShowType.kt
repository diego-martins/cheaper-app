package com.onpister.cheaper

enum class ShowType {
    NONE,
    COMPARE,
    BEST
}