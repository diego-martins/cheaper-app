package com.onpister.cheaper.utils

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import com.google.android.gms.ads.AdRequest
import com.onpister.cheaper.*
import java.math.BigDecimal
import java.text.NumberFormat
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.view.View

class Utils
{
    object Ad{

        fun adRequest(): AdRequest {
            val builder = AdRequest.Builder()
            if (BuildConfig.DEBUG) {
                builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                builder.addTestDevice("594A430AA4F7F100961B3B4ED7F41F23")
                builder.addTestDevice("521A0AD71D4BBF5A968602F2972391C7")
                builder.addTestDevice("C3A88DA00048C3E14166A6779500C6F7")
            }
            return builder.build()
        }

    }

    object Text{

        fun formattString(str: String): String {
            var res: String = if(!str.isNullOrEmpty()) str.replace(Regex("(\\D+|!\\.,)"), "") else "0"
            res = res.padStart(3, '0')
            val nf = NumberFormat.getCurrencyInstance()
            try{
                var value: Double = res.toDouble()
                return nf.format(value/100)
            }catch (e:NumberFormatException){
            }
            return str;
        }

        fun formattString(value: Double): String {
            var str: String = BigDecimal(value as Double).setScale(2, BigDecimal.ROUND_HALF_UP).toDouble().toString()
            var res: String = if(!str.isNullOrEmpty()) str.replace(Regex("(\\D+|!\\.,)"), "") else "0"
            res = res.padEnd(3, '0')
            val nf = NumberFormat.getCurrencyInstance()
            try{
                var value: Double = res.toDouble()
                return nf.format(value/100)
            }catch (e:NumberFormatException){
            }
            return str;
        }

        fun formattDecimal(str: String): Double {
            var res: String = if(!str.isNullOrEmpty()) str.replace(Regex("\\D+"), "") else "0"
            var value: Double = res.toDouble()/100
            return value
        }

    }

    object Network {
        fun isOnline(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnectedOrConnecting
        }
    }

    object Calc {
        fun result(context: Context, gas: Double, ethanol: Double, factor: Double): String{
            if(gas.compareTo(0) > 0 && ethanol.compareTo(0) > 0){
                var res: Double = ethanol/gas
                if(res <= factor){
                    return context.getString(R.string.msg_use_ethanol) + String.format(" (%.1f%%)", res*100)
                }else{
                    return context.getString(R.string.msg_use_gasoline)+ String.format(" (%.1f%%)", res*100)
                }
            }
            else if(gas.compareTo(0) > 0){
                return context.getString(R.string.msg_use_ethanol_only, Utils.Text.formattString(gas*factor)) + String.format(" (%.1f%%)", factor*100)
            }
            else if(ethanol.compareTo(0) > 0){
                return context.getString(R.string.msg_use_gasoline_only, Utils.Text.formattString(ethanol/factor))+ String.format(" (%.1f%%)", factor*100)
            }
            return ""
        }

        fun result(gas: BaseFuel, ethanol: BaseFuel): BaseFuel{
            var res = BaseFuel()
            if(gas.price.compareTo(0) > 0 && ethanol.price.compareTo(0) > 0){
                res.percentCheaper = ethanol.price/gas.price
                if(res.percentCheaper < ethanol.percentCheaper){
                    res.fuelType = FuelType.ETHANOL
                    res.percentCheaper = res.percentCheaper *100
                }else if(res.percentCheaper > ethanol.percentCheaper){
                    res.fuelType = FuelType.GASOLINE
                    res.percentCheaper = res.percentCheaper *100
                }else{
                    res.fuelType = FuelType.NONE
                    res.percentCheaper = res.percentCheaper *100
                }
            }
            else if(gas.price.compareTo(0) > 0){
                res.fuelType = FuelType.ETHANOL
                res.price = gas.price*ethanol.percentCheaper
                res.percentCheaper = ethanol.percentCheaper *100
            }
            else if(ethanol.price.compareTo(0) > 0){
                res.fuelType = FuelType.GASOLINE
                res.price = ethanol.price/ethanol.percentCheaper
                res.percentCheaper = ethanol.percentCheaper *100
            }
            return res
        }

        fun result(gas: BaseFuel, ethanol: BaseFuel, gnv: BaseFuel, autonomy: Autonomy): ArrayList<BaseFuel>{
            var res = ArrayList<BaseFuel>()

            if(gas.price.compareTo(0) > 0 && ethanol.price.compareTo(0) > 0 && gnv.price.compareTo(0) > 0){

                res.add(0, BaseFuel())
                var diff = ethanol.price/gas.price
                if(diff <= autonomy.ethToGas){

                    diff = gnv.price/ethanol.price
                    if(diff <= autonomy.gnvToEth){
                        res[0].price = gnv.price
                        res[0].fuelType = FuelType.GNV
                        res[0].betterThen.add(Pair(FuelType.ETHANOL, diff))
                    }else{
                        res[0].price = ethanol.price
                        res[0].fuelType = FuelType.ETHANOL
                        res[0].betterThen.add(Pair(FuelType.GNV, diff))
                    }

                }else {

                    diff = gnv.price/gas.price
                    if(diff <= autonomy.gnvToGas){
                        res[0].price = gnv.price
                        res[0].fuelType = FuelType.GNV
                        res[0].betterThen.add(Pair(FuelType.GASOLINE, diff))
                    }else{
                        res[0].price = gas.price
                        res[0].fuelType = FuelType.GASOLINE
                        res[0].betterThen.add(Pair(FuelType.GNV, diff))
                    }

                }
                res[0].showType = ShowType.COMPARE

            }
            else if(gas.price.compareTo(0) > 0 && ethanol.price.compareTo(0) > 0){

                res.add(0, BaseFuel())
                var diff = ethanol.price/gas.price
                if(diff <= autonomy.ethToGas){
                    res[0].price = ethanol.price
                    res[0].fuelType = FuelType.ETHANOL
                    res[0].betterThen.add(Pair(FuelType.GASOLINE, diff))
                }else {
                    res[0].price = gas.price
                    res[0].fuelType = FuelType.GASOLINE
                    res[0].betterThen.add(Pair(FuelType.ETHANOL, diff))
                }
                res[0].showType = ShowType.COMPARE

            }
            else if(gnv.price.compareTo(0) > 0 && gas.price.compareTo(0) > 0){

                res.add(0, BaseFuel())
                var diff = gnv.price/gas.price
                if(diff <= autonomy.gnvToGas){
                    res[0].price = gnv.price
                    res[0].fuelType = FuelType.GNV
                    res[0].betterThen.add(Pair(FuelType.GASOLINE, diff))
                }else{
                    res[0].price = gas.price
                    res[0].fuelType = FuelType.GASOLINE
                    res[0].betterThen.add(Pair(FuelType.GNV, diff))
                }
                res[0].showType = ShowType.COMPARE

            }
            else if(gnv.price.compareTo(0) > 0 && ethanol.price.compareTo(0) > 0){

                res.add(0, BaseFuel())
                var diff = gnv.price/ethanol.price
                if(diff <= autonomy.gnvToEth){
                    res[0].price = gnv.price
                    res[0].fuelType = FuelType.GNV
                    res[0].betterThen.add(Pair(FuelType.ETHANOL, diff))
                }else{
                    res[0].price = ethanol.price
                    res[0].fuelType = FuelType.ETHANOL
                    res[0].betterThen.add(Pair(FuelType.GNV, diff))
                }
                res[0].showType = ShowType.COMPARE

            }
            else if(gas.price.compareTo(0) > 0){

                res.add(0, BaseFuel())
                res[0].fuelType = FuelType.ETHANOL
                res[0].price = gas.price*autonomy.ethToGas
                res[0].betterThen.add(Pair(FuelType.GASOLINE, autonomy.ethToGas))
                res[0].showType = ShowType.BEST

                res.add(1, BaseFuel())
                res[1].fuelType = FuelType.GNV
                res[1].price = gas.price*autonomy.gnvToGas
                res[1].betterThen.add(Pair(FuelType.GASOLINE, autonomy.gnvToGas))
                res[1].showType = ShowType.BEST

            }
            else if(ethanol.price.compareTo(0) > 0){

                res.add(0, BaseFuel())
                res[0].fuelType = FuelType.GASOLINE
                res[0].price = ethanol.price/autonomy.ethToGas
                res[0].betterThen.add(Pair(FuelType.ETHANOL, autonomy.ethToGas))
                res[0].showType = ShowType.BEST

                res.add(1, BaseFuel())
                res[1].fuelType = FuelType.GNV
                res[1].price = ethanol.price*autonomy.gnvToEth
                res[1].betterThen.add(Pair(FuelType.ETHANOL, autonomy.gnvToEth))
                res[1].showType = ShowType.BEST

            }
            else if(gnv.price.compareTo(0) > 0){

                res.add(0, BaseFuel())
                res[0].fuelType = FuelType.GASOLINE
                res[0].price = gnv.price/autonomy.gnvToGas
                res[0].betterThen.add(Pair(FuelType.GNV, autonomy.gnvToGas))
                res[0].showType = ShowType.BEST

                res.add(1, BaseFuel())
                res[1].fuelType = FuelType.ETHANOL
                res[1].price = gnv.price/autonomy.gnvToEth
                res[1].betterThen.add(Pair(FuelType.GNV, autonomy.gnvToEth))
                res[1].showType = ShowType.BEST

            }

            return res
        }
    }

    object Media {
        fun openWeb(context: Context, url: String) {
            val intentWeb = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(intentWeb)
        }

        fun getShareIntent(context: Context, text: String): Intent {
            return getShareIntent(context, "", text)
        }

        fun getShareIntent(context: Context, title: String, text: String): Intent {
            var title = title
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"

            if (!title.isNullOrEmpty()) {
                intent.putExtra(Intent.EXTRA_TITLE, title)
            }

            intent.putExtra(Intent.EXTRA_TEXT, text)
            return Intent.createChooser(intent, context.getString(R.string.share_via))
        }

        fun getShareIntent(context: Context, title: String, text: String, uri: Uri): Intent {

            if(uri == null) return getShareIntent(context, title, text)

            var title = title
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = context.contentResolver.getType(uri)

            if (!title.isNullOrEmpty()) {
                intent.putExtra(Intent.EXTRA_TITLE, title)
            }

            intent.putExtra(Intent.EXTRA_EMAIL, "")
            intent.putExtra(Intent.EXTRA_TEXT, text)
            intent.putExtra(Intent.EXTRA_STREAM, uri)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            //intent.setDataAndType(uri, context.contentResolver.getType(uri))
            return Intent.createChooser(intent, context.getString(R.string.share_via))
        }
    }

    object Image {
        fun viewToImage(view: View): Bitmap{
            val returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(returnedBitmap)
            val bgDrawable = view.getBackground()

            if (bgDrawable != null) {
                bgDrawable!!.draw(canvas)
            }
            else {
                canvas.drawColor(Color.WHITE)
            }

            view.draw(canvas)
            return returnedBitmap
        }
    }
}